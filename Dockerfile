FROM registry.gitlab.com/santiagoibarzabal/ci-mdas:master

WORKDIR /app
COPY nginx.conf /etc/nginx/conf.d/mdas.conf
COPY --chown=www-data . .

# Preparing composer
RUN composer install

# Link project to nginx
RUN rm -rf /usr/share/nginx/html; \
    ln -s /app/public /usr/share/nginx/html; \
    chown -R www-data:www-data /usr/share/nginx/html; \
    chown -R www-data:www-data /app
